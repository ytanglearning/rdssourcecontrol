﻿CREATE TABLE [dbo].[CustomerType] (
    [CustomerTypeId] BIGINT       IDENTITY (1, 1) NOT NULL,
    [Description]    VARCHAR (50) NOT NULL,
    [CreateDate]     DATETIME     CONSTRAINT [DF_CustomerType_CreateDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CustomerType] PRIMARY KEY CLUSTERED ([CustomerTypeId] ASC)
);

