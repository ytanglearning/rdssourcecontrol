﻿CREATE TABLE [dbo].[Invoice] (
    [InvoiceId]     BIGINT          IDENTITY (1, 1) NOT NULL,
    [CustomerId]    BIGINT          NOT NULL,
    [InvoiceAmount] DECIMAL (18, 2) NOT NULL,
    [PaidDate]      DATETIME        NULL,
    [CreatedDate]   DATETIME        CONSTRAINT [DF_Invoice_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [DeletedDate]   DATETIME        NULL,
    CONSTRAINT [PK_Invoice] PRIMARY KEY CLUSTERED ([InvoiceId] ASC),
    CONSTRAINT [FK_Invoice_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId])
);

