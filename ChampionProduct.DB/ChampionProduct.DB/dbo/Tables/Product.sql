﻿CREATE TABLE [dbo].[Product] (
    [ProductId]   INT          IDENTITY (1, 1) NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    [CreateDate]  DATETIME     CONSTRAINT [DF_Product_CreateDate] DEFAULT (getdate()) NOT NULL,
    [IsEnabled]   BIT          NOT NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([ProductId] ASC)
);

