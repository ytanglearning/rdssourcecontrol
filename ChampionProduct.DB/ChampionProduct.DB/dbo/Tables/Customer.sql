﻿CREATE TABLE [dbo].[Customer] (
    [CustomerId]     BIGINT          IDENTITY (1, 1) NOT NULL,
    [CustomerTypeId] BIGINT          NOT NULL,
    [BusinessName]   VARBINARY (200) NULL,
    [LastName]       VARCHAR (50)    NOT NULL,
    [FirstName]      VARCHAR (50)    NULL,
    [AddressId]      INT             NULL,
    [CreateDate]     DATETIME        CONSTRAINT [DF_Customer_CreateDate] DEFAULT (getdate()) NOT NULL,
    [IsEnabled]      BIT             NOT NULL,
    CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([CustomerId] ASC),
    CONSTRAINT [FK_Customer_CustomerType] FOREIGN KEY ([CustomerTypeId]) REFERENCES [dbo].[CustomerType] ([CustomerTypeId])
);

