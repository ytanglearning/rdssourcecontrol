﻿-- =============================================
-- Author:		Yuezhe Tang
-- Create date: 04/18/2021
-- Description:	Get a list of Customer Types
-- Change log:
-- 
-- SAMPLE CALL;
/*

DECLARE 
	@CustomerTypeId BIGINT = 1

	
EXEC dbo.Customer_Type_Get_List
	@CustomerTypeId = @CustomerTypeId

 */
 
-- SELECT * from dbo.CustomerType
-- =============================================
CREATE PROCEDURE [dbo].[Customer_Type_Get_List]
	-- Add the parameters for the stored procedure here
	@CustomerTypeId BIGINT = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Fetch results.
	-- <Summary>
	-- </Summary>
	--------------------------------------------------------------------------------------------
    -- Insert statements for procedure here
	IF @CustomerTypeId IS NOT NULL
	BEGIN
		SELECT * from dbo.CustomerType WHERE CustomerTypeId = @CustomerTypeId
	END
	ELSE
	BEGIN
		SELECT * from dbo.CustomerType
	END
END